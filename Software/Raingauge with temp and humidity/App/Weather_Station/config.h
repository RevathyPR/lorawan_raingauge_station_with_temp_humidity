/*
 * config.h
 *
 *  Created on: 30-Jul-2021
 *      Author: Ajmi
 *      @about A single config file to configure the lorawan parameters without editing the main files.
 */

#ifndef WEATHER_STATION_CONFIG_H_
#define WEATHER_STATION_CONFIG_H_

#ifndef OVER_THE_AIR_ACTIVATION
#define OVER_THE_AIR_ACTIVATION                           0
#endif



/************************************Extra features that can be enabled here**********************************/


/************************************Extra features that can be enabled here**********************************/


/************************************Device Key Configuration***************************************/

#define DEVICE_EUI 							{0x65, 0xe1, 0xbe, 0xe1, 0xd8, 0x32, 0xad, 0x77}

#define JOIN_EUI                            {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
#define OTAA_APP_KEY						{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

#if !OVER_THE_AIR_ACTIVATION

#define DEVICE_ADDRESS 						( uint32_t )0xfc009778
#define NETWORK_SESSION_KEY					{0x71, 0x47, 0x22, 0x1D, 0x6E, 0x46, 0x38, 0x0F, 0x13, 0xA2, 0xEE, 0x20, 0x79, 0xF3, 0x49, 0x79}
#define APP_SESSION_KEY						{0x78, 0x26, 0xE9, 0x4D, 0xD6, 0x0D, 0x7F, 0x16, 0xB8, 0x33, 0x3E, 0x0F, 0x5B, 0x80, 0x24, 0x7F}

#endif

/************************************Device Key Configuration***************************************/



/************************************Device Operation Configuration***************************************/

#define SEND_INTERVAL						900000              /*the application data transmission duty cycle*/

#define ADR_STATE 							LORAWAN_ADR_OFF	   /*LoRaWAN Adaptive Data Rate * @note Please note that when ADR is enabled the end-device should be static  check commissioning.h*/

#define DATA_RATE							DR_3

#define APP_PORT							2

#define CLASS_OPERATION						CLASS_A

#define UPLINK_MSG_STATE					LORAWAN_UNCONFIRMED_MSG

/************************************Device Operation Configuration***************************************/



#endif /* WEATHER_STATION_CONFIG_H_ */
